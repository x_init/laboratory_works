import sys

N = int(input(':'))

if not (N >= 1 and N <= 30):
	sys.exit()

#N = 2*N - 1

for i in range(2*N - 1):
	for j in range(N):
		if i == j or j == (2 * N - 1) - 1 - i or j == 0:
			print('*', end = '')
		else:
			print(' ', end = '')
	print()


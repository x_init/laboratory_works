import matplotlib.pyplot as plt
import numpy as np

fo = open('people.txt', 'r')
text = fo.read()
fo.close()
text = [ g.split(';') for g in text.split('\n') if len(g) > 0]
text = text[2::]
group = []
men = [] 
women = []

for element in text:
	element[1] = int(element[1])
	element[2] = int(element[2])
	element[3] = int(element[3])
	group.append(element[0])
	men.append(element[2])
	women.append(element[3])
	#print(element)
#print(group)
#print(men)
#print(women)
#x = np.linspace(-5, 5, 100)
#y = 9.5*x**31
#ax.plot(x, y)
#ax.vlines()
#ax.hlines()
#создается массив
x = np.arange(len(group))
# ширина стержней
width = 0.4 
fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, men, width, label='Мужчины', color='#00FF00')
rects2 = ax.bar(x + width/2, women, width, label='Женщины', color='#FF0000')

ax.set_ylabel('Численность населения', fontsize=7)
ax.set_title('Результаты по группам и полу', fontsize=7)
ax.set_xticks(x)
ax.set_xticklabels(group, fontsize=7)
ax.tick_params(labelsize=5)
ax.legend()


def autolabel(rects):
	for rect in rects:
		height = rect.get_height()
		ax.annotate(
			'{}'.format(height),
			xy=(rect.get_x() + rect.get_width() / 2, height),
			xytext=(0, 3),  # 3 points vertical offset
			textcoords="offset points",
			ha='center', va='bottom',
			size=5
			)

#подписи численности мужчин и женщин
autolabel(rects1)
autolabel(rects2)

#пропорции
fig.set_figwidth(9)
fig.set_figheight(5)
fig.tight_layout()

plt.show()






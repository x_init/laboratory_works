import pygame
from init import *
# при падении на ДНО блок получает заливку и рождается следующий БЛОК


pygame.init()
pygame.display.set_caption('tetRIS')
win = pygame.display.set_mode(sc_size)
win.fill(color_bg)

colors = [(225, 125, 0), (0, 225, 125), (0, 125, 225)]  # цвета блоков
index_color = 0  # начальный цвет

pos_x = cnt_w // 2
pos_y = 0
pygame.draw.rect(win, colors[index_color], (pos_x*r, pos_y*r, r, r), 3)
pygame.display.update()

clock = pygame.time.Clock()
fall_time = 0
interval = 300  # mlsek
press_time = 0
int_pressed = 100  # mlsek

pause = 1
acc = 0
matrx = []
for _ in range(cnt_h):
	matrx.append([int(el) for el in list('0'*cnt_w)])

while True:
	for e in pygame.event.get():
		if e.type == pygame.QUIT: 
			pygame.quit()
		if e.type == pygame.KEYUP:
			if e.key == pygame.K_SPACE:
				for el in matrx:
					print(el)
				pause *= -1
		if e.type == pygame.KEYUP:
			if e.key == pygame.K_DOWN:
				for i in range(cnt_h):
					if matrx[i][pos_x] == 1:
						pygame.draw.rect(win, color_bg, (pos_x * r, pos_y * r, r, r), 3) 
						pos_y = i - 1
						break
					elif matrx[cnt_h-1][pos_x] == 0:
						pygame.draw.rect(win, color_bg, (pos_x * r, pos_y * r, r, r), 3) 
						pos_y = cnt_h - 1
						break
	if pause == 1:
		clock.tick()
		fall_time += clock.get_rawtime()
		press_time += clock.get_rawtime()
		if press_time >= int_pressed:
			press_time = 0
			key = pygame.key.get_pressed()  # checking pressed keys
			if pos_y < cnt_h - 1:  # на дне стакана нельзя двигать блок
				if pos_x > 0 and key[pygame.K_LEFT] and matrx[pos_y][pos_x-1] != 1:
					pygame.draw.rect(win, color_bg, (pos_x * r, pos_y * r, r, r), 3)
					pos_x -= 1
				if pos_x < cnt_w - 1 and key[pygame.K_RIGHT] and matrx[pos_y][pos_x+1] != 1:
					pygame.draw.rect(win, color_bg, (pos_x * r, pos_y * r, r, r), 3)
					pos_x += 1
		if fall_time >= interval:
			fall_time = 0
			if pos_y < cnt_h - 1:
				pygame.draw.rect(win, color_bg, (pos_x * r, pos_y * r, r, r), 3)
				pos_y += 1
		if matrx[1][pos_x] == 1:
			break
		if pos_y < cnt_h - 1 and matrx[pos_y+1][pos_x] != 1:
			pygame.draw.rect(win, colors[index_color], (pos_x * r, pos_y * r, r, r), 3) 
			# контур квадрата
		else:
			pygame.draw.rect(win, (random.randint(0,256),random.randint(0,256),random.randint(0,256)), (pos_x * r, pos_y * r, r, r))  
			matrx[pos_y][pos_x] = 1
			# заливка квадрата
			pos_y = 0
			pos_x = cnt_w // 2
			index_color = (index_color + 1) % len(colors)
		pygame.display.update()

font = pygame.font.SysFont('Bront', 20)
win.fill(color_bg)
win.blit(font.render('Game over!', True, color_fg), (0, 0))
pygame.display.update()


